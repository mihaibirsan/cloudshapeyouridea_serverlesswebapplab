$(function () {

$('.form-transformer').on('submit', function (event) {
  // act on the input value
  console.warn($('#inputText').val();

  // clear the input
  $('#inputText').val('').focus();

  // don't actually submit the form
  event.preventDefault();
  return false;
});

});
